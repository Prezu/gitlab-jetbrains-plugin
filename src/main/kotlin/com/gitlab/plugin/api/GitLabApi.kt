package com.gitlab.plugin.api

import com.gitlab.plugin.GitLabBundle
import com.intellij.collaboration.util.resolveRelative
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.application.ApplicationNamesInfo
import com.intellij.openapi.util.SystemInfo
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.util.network.*
import kotlinx.serialization.json.Json
import java.net.URI

private const val DEFAULT_SERVER_URL = "https://gitlab.com"

class GitLabUnauthorizedException(response: HttpResponse, cachedResponseText: String) :
  ResponseException(response, cachedResponseText) {
  override val message: String = "Invalid authentication for Request: ${response.call.request.url}, " +
    "Method: ${response.call.request.method.value}, Status: ${response.status}."
}

class GitLabApi(val token: String, gitlabHost: String = DEFAULT_SERVER_URL) {
  private val baseUrl : URI
  private val version = GitLabBundle.plugin()?.version ?: "DEV"

  init {
    baseUrl = URI(gitlabHost)
  }

  val restClient = HttpClient(CIO) {
    expectSuccess = true

    install(UserAgent) {
      agent = getUserAgent()
    }
    install(HttpRequestRetry) {
      retryOnServerErrors(maxRetries = 3)
      exponentialDelay()
    }
    install(Auth) {
      bearer {
        loadTokens {
          BearerTokens(token, "")
        }
      }
    }
    install(ContentNegotiation) {
      json(Json {
        isLenient = true
      })
    }

    HttpResponseValidator {
      handleResponseExceptionWithRequest { exception, _ ->
        val clientException = exception as? ClientRequestException ?: return@handleResponseExceptionWithRequest
        val exceptionResponse = clientException.response

        if (exceptionResponse.status == HttpStatusCode.Unauthorized) {
          val exceptionResponseText = exceptionResponse.bodyAsText()
          throw GitLabUnauthorizedException(exceptionResponse, exceptionResponseText)
        }
      }
    }
  }

  val apiUri: URI
    get() = baseUrl.resolveRelative("api/v4/")

  /**
   * Return whether is correctly configured to perform requests
   */
  fun isConfigured() : Boolean {
    return token.isNotEmpty() && baseUrl.toString().isNotEmpty()
  }

  @Throws(GitLabUnauthorizedException::class)
  suspend fun generateCodeSuggestionsToken(): CodeSuggestionsTokenDto {
    val uri = apiUri.resolveRelative("code_suggestions/tokens").toString()
    val response: HttpResponse = restClient.post(uri) {
      contentType(ContentType.Application.Json)
    }

    return response.body<CodeSuggestionsTokenDto>()
  }

  private fun getUserAgent(): String {
    return getUserAgentValue("gitlab-jetbrains-plugin/$version")
  }

  /**
   * Extracted from HttpClientUtils (EAP)
   * Copyright 2000-2022 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license.
   */
  private fun getUserAgentValue(agentName: String): String {
    val ideName = ApplicationNamesInfo.getInstance().fullProductName.replace(' ', '-')
    val ideBuild = ApplicationInfo.getInstance().build.asString()
    val java = "JRE " + SystemInfo.JAVA_RUNTIME_VERSION
    val os = SystemInfo.OS_NAME + " " + SystemInfo.OS_VERSION
    val arch = SystemInfo.OS_ARCH

    return "$agentName $ideName/$ideBuild ($java; $os; $arch)"
  }
}
