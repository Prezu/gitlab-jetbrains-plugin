package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.gitlab.plugin.util.Status
import com.gitlab.plugin.util.TokenUtil
import com.intellij.openapi.actionSystem.DataContext
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.popup.ListPopup
import com.intellij.openapi.util.IconLoader
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.wm.StatusBarWidget
import com.intellij.openapi.wm.impl.status.EditorBasedStatusBarPopup
import com.intellij.util.messages.MessageBusConnection
import java.util.*

class GitLabStatusPanel(project: Project) :
  EditorBasedStatusBarPopup(project = project, isWriteableFileRequired = false) {

  var status: Status = Status.Disabled
  private val bundle: ResourceBundle = ResourceBundle.getBundle("messages.GitLabBundle")

  init {
    if (TokenUtil.getToken()?.isNotEmpty() == true) {
      status = Status.Enabled
    }
  }

  override fun ID(): String = WIDGET_ID

  override fun createInstance(project: Project): StatusBarWidget = GitLabStatusPanel(project)

  override fun createPopup(context: DataContext): ListPopup? = null

  override fun getWidgetState(file: VirtualFile?): WidgetState = getGitLabState()

  override fun registerCustomListeners(connection: MessageBusConnection) {
    connection.subscribe(GitLabSettingsListener.SETTINGS_CHANGED, object : GitLabSettingsListener {
      override fun codeStyleSettingsChanged(event: GitLabSettingsChangeEvent) {
        status = event.status

        update()
      }
    })
  }

  private fun getGitLabState(): WidgetState {
    val iconPath: String
    val tooltipId: String
    when (status) {
      Status.Enabled -> {
        tooltipId = "status-icon.tooltip.enabled"
        iconPath = "/icons/gitlab-code-suggestions-enabled.svg"
      }
      Status.Disabled -> {
        tooltipId = "status-icon.tooltip.disabled"
        iconPath = "/icons/gitlab-code-suggestions-disabled.svg"
      }
      Status.Error -> {
        tooltipId = "status-icon.tooltip.error"
        iconPath = "/icons/gitlab-code-suggestions-error.svg"
      }
      Status.Loading -> {
        tooltipId = "status-icon.tooltip.loading"
        iconPath = "/icons/gitlab-code-suggestions-loading.svg"
      }
    }

    val widgetState = WidgetState(bundle.getString(tooltipId), "", true)
    widgetState.icon = IconLoader.getIcon(iconPath, javaClass)

    return widgetState
  }

  override fun afterVisibleUpdate(state: WidgetState) {
    super.afterVisibleUpdate(state)
    component.repaint()
  }

  companion object {
    @JvmField
    val WIDGET_ID: String = GitLabStatusPanel::class.java.name
  }
}
