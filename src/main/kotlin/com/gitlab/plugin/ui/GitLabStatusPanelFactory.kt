package com.gitlab.plugin.ui

import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.StatusBarWidget
import com.intellij.openapi.wm.StatusBarWidgetFactory

class GitLabStatusPanelFactory : StatusBarWidgetFactory {
  override fun getDisplayName(): String = "GitLab Status"

  override fun getId(): String = GitLabStatusPanel.WIDGET_ID

  override fun createWidget(project: Project): StatusBarWidget = GitLabStatusPanel(project)
}
