package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.util.GitLabUtil
import com.gitlab.plugin.util.JwtSession
import com.gitlab.plugin.util.Status
import com.gitlab.plugin.util.gitlabStatusChangedNotify
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.intellij.codeInsight.inline.completion.InlineCompletionContext.Companion.initOrGetInlineCompletionContext
import com.intellij.codeInsight.inline.completion.InlineCompletionElement
import com.intellij.codeInsight.inline.completion.InlineCompletionProvider
import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.editor.event.DocumentEvent
import com.intellij.openapi.progress.ProcessCanceledException
import com.intellij.openapi.progress.ProgressIndicatorProvider
import com.intellij.util.io.HttpRequests
import org.jetbrains.annotations.ApiStatus
import java.io.IOException
import java.net.HttpRetryException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference

@ApiStatus.Experimental
class CodeSuggestionsProvider : InlineCompletionProvider {

  private val gson: Gson = GsonBuilder()
    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
    .create()
  private val completionListRef = AtomicReference<List<String>>()
  private val requestCounter = AtomicInteger(0)
  private val logger = Logger.getInstance(CodeSuggestionsProvider::class.java)
  private var showAuthNotification = true
  private var jwtToken = ""

  private val circuitBreaker = CircuitBreaker(
    GitLabUtil.MAX_ERRORS_BEFORE_CIRCUIT_BREAK, GitLabUtil.CIRCUIT_BREAK_INTERVAL_MS,
  )

  override suspend fun getProposals(request: InlineCompletionRequest): List<InlineCompletionElement> {
    if (!GitLabUtil.isSupportedFileExtension(request.file.fileType.defaultExtension)) {
      return emptyList()
    }

    val context = request.editor.initOrGetInlineCompletionContext()
    if (!context.isCurrentlyDisplayingInlays) {
      try {
        val completionList = fetchCompletion(request)
        if (completionList.isNotEmpty()) {
          return listOf(
            InlineCompletionElement(completionList[0])
          )
        }
      } catch (e: Exception) {
        handleException(e)
        return emptyList()
      }
    }
    return emptyList()
  }

  override fun isEnabled(event: DocumentEvent): Boolean {
    if (this.circuitBreaker.isBreaking()) return false
    jwtToken = fetchJwt()
    return jwtToken.isNotEmpty()
  }

  private fun fetchJwt(): String {
    var token = ""
    try {
      token = JwtSession.getInstance().getJWT()
    } catch (ex: GitLabUnauthorizedException) {
      this.circuitBreaker.error()
      if (showAuthNotification) {
        GitLabNotificationManager().sendNotification(
          Notification(
            "GitLab",
            "User is unauthorized to use code suggestions."
          ), null
        )
        showAuthNotification = false
      }
    }
    return token
  }

  private data class CodeSuggestionRequest(
    val promptVersion: Int,
    val projectPath: String,
    val projectId: Int,
    val currentFile: CodeSuggestionRequestFile
  )

  private data class CodeSuggestionRequestFile(
    val fileName: String,
    val contentAboveCursor: String,
    val contentBelowCursor: String
  )

  private data class CodeSuggestionResponse(
    val choices: Array<CodeSuggestionResponseChoice>
  )

  private data class CodeSuggestionResponseChoice(
    val text: String
  )

  private fun fetchCompletion(request: InlineCompletionRequest): List<String> {
    if (this.circuitBreaker.isBreaking()) return emptyList()

    val document = request.document
    val content = document.charsSequence

    val currentPosition: Int = request.startOffset + 1

    if (document.textLength > content.length
      || currentPosition < 0
      || currentPosition > document.textLength
    ) return emptyList()

    val aboveCursor = content.take(currentPosition).toString()
    val belowCursor = content.drop(currentPosition).toString()

    val latch = CountDownLatch(1)

    val prompt = gson.toJson(
      CodeSuggestionRequest(
        1,
        "",
        -1,
        CodeSuggestionRequestFile(
          request.file.name,
          aboveCursor,
          belowCursor
        )
      )
    )

    gitlabStatusChangedNotify(Status.Loading)

    try {
      ApplicationManager.getApplication().runReadAction {
        ApplicationManager.getApplication().executeOnPooledThread {
          val currentRequestNumber = requestCounter.incrementAndGet()

          try {
            HttpRequests.post("https://codesuggestions.gitlab.com/v2/completions", "application/json")
              .productNameAsUserAgent()
              .tuner { connection ->
                connection.setRequestProperty("Authorization", "Bearer $jwtToken")
                connection.setRequestProperty("X-Gitlab-Authentication-Type", "oidc")
              }
              .connect { connection ->
                connection.write(prompt)

                this.circuitBreaker.success()

                val response = connection.readString(ProgressIndicatorProvider.getGlobalProgressIndicator())
                val completion = gson.fromJson(response, CodeSuggestionResponse::class.java)
                if (!completion.choices.isEmpty() && currentRequestNumber >= requestCounter.get()) {
                  completionListRef.set(completion.choices.map { it.text })
                }
                gitlabStatusChangedNotify(Status.Enabled)

                return@connect null
              }

          } catch (e: HttpRetryException) {
            handleException(e, true, true)
          } catch (e: IOException) {
            handleException(e)
          } finally {
            latch.countDown() // Release the latch, indicating that the operation is completed
            if (currentRequestNumber >= requestCounter.get()) {
              requestCounter.set(currentRequestNumber)
            }
          }
        }
      }
    } catch (e: ProcessCanceledException) {
      // no operation
    } catch (e: Exception) {
      handleException(e, false)
      throw RuntimeException(e)
    }

    // Wait for the latch to be released, blocking the current thread
    latch.await()

    return completionListRef.get() ?: emptyList()
  }

  private fun handleException(e: Exception, logException: Boolean = true, refreshJwt: Boolean = false) {
    if (refreshJwt) JwtSession.getInstance().refreshJWT()
    this.circuitBreaker.error()
    gitlabStatusChangedNotify(Status.Error)
    if (logException) logger.error("Exception while fetching suggestions.", e)
  }
}
