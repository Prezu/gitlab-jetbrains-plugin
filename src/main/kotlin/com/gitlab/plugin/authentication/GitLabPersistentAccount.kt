package com.gitlab.plugin.authentication

import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.components.service
import com.intellij.util.application


@State(name = "GitLabAccount", storages = [Storage(value = "gitlab.xml")], reportStatistic = false)
class GitLabPersistentAccount : PersistentStateComponent<GitLabPersistentAccount.State> {
  data class State(
    var url: String = GitLabUtil.GITLAB_DEFAULT_URL,
    var enabled: Boolean = false
  )

  private var state: State = State()

  override fun getState(): State = state

  override fun loadState(state: State) {
    this.state = state
  }

  companion object {
    fun getInstance(): GitLabPersistentAccount = application.service()
  }
}
