package com.gitlab.plugin.util

import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.intellij.openapi.application.ApplicationManager

object GitLabUtil {
  const val SERVICE_NAME = "com.gitlab.plugin"
  const val GITLAB_DEFAULT_URL = "https://gitlab.com"
  const val CIRCUIT_BREAK_INTERVAL_MS = 10000
  const val MAX_ERRORS_BEFORE_CIRCUIT_BREAK = 4

  private val SUPPORTED_EXTENSIONS = setOf(
    "cpp", "c", "h", // C++
    "cs", // C#
    "go", // Go
    "gsql", // Google SQL
    "java", // Java
    "js", // JavaScript
    "kt", // Kotlin
    "php", // PHP
    "py", // Python
    "rb", // Ruby
    "rs", // Rust
    "scala", // Scala
    "swift", // Swift
    "ts" // TypeScript
  )

  fun isValidURL(url: String): Boolean {
    /* Below regex checks only if the url starts with http:// or https:// */
    val pattern = Regex("^(https?://)?(www\\.)?\\w+\\.[a-zA-Z]{2,}(\\.\\w+)?.*")
    return pattern.matches(url)
  }

  fun isSupportedFileExtension(fileExtension: String): Boolean {
    return fileExtension in SUPPORTED_EXTENSIONS
  }
}

enum class Status {
  Enabled,
  Disabled,
  Error,
  Loading,
}

fun gitlabStatusChangedNotify(status: Status) {
  ApplicationManager.getApplication().messageBus.syncPublisher(GitLabSettingsListener.SETTINGS_CHANGED)
    .codeStyleSettingsChanged(GitLabSettingsChangeEvent(status))
}
