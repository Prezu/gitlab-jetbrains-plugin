package com.gitlab.plugin.util

import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TokenUtilTest : BasePlatformTestCase() {
  var receivedEvent: MutableList<GitLabSettingsChangeEvent> = mutableListOf()

  @BeforeEach
  public override fun setUp() {
    super.setUp()

    project.messageBus.connect(testRootDisposable).subscribe(
      GitLabSettingsListener.SETTINGS_CHANGED,
      object : GitLabSettingsListener {
        override fun codeStyleSettingsChanged(event: GitLabSettingsChangeEvent) {
          receivedEvent.add(event)
        }
      }
    )
  }

  @Test
  fun sendEnabledEvent() {
    val token = "dummyToken"

    // Action
    TokenUtil.setToken(token)

    val receivedEvent = receivedEvent.last()
    assertEquals(Status.Enabled, receivedEvent.status)
    assertEquals(token, TokenUtil.getToken())
  }

  @Test
  fun sendDisabledEvent() {
    val token = ""

    // Action
    TokenUtil.setToken(token)

    val receivedEvent = receivedEvent.last()
    assertEquals(Status.Disabled, receivedEvent.status)
    assertEquals(token, TokenUtil.getToken())
  }
}

