## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

All Documentation content that resides under the [docs/ directory](/docs) of this
repository is licensed under Creative Commons:
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

_This notice should stay as the first item in the CONTRIBUTING.md file._

---

# Contributing to the GitLab Plugin for JetBrains

Thank you for your interest in contributing to the GitLab Plugin for JetBrains! This guide details how to contribute
to this plugin in a way that is easy for everyone. These are mostly guidelines, not rules.
Use your best judgement, and feel free to propose changes to this document in a merge request.

## Code of Conduct

We want to create a welcoming environment for everyone who is interested in contributing. Visit our [Code of Conduct page](https://about.gitlab.com/community/contribute/code-of-conduct/) to learn more about our commitment to an open and welcoming environment.

## Getting Started

### Reporting Issues

Create a [new issue from the "Bug" template](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new?issuable_template=Bug) and follow the instructions in the template.

### Proposing Features

Create a [new issue from the "Feature Proposal" template](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new?issuable_template=Feature%20Proposal) and follow the instructions in the template.

### Your First Code Contribution?

For newcomers to the project, you can take a look at issues labelled as `Accepting merge requests`
as available [here](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-pluginn/-/issues?label_name[]=Accepting%20merge%20requests).

### Configuring Development Environment

The following instructions will help you run the GitLab Plugin locally.

#### Step - 1 : Installation Prerequisites

We're assuming that you already have [IntelliJ IDEA CE](https://www.jetbrains.com/idea/download)
or [IntelliJ IDEA Ultimate](https://www.jetbrains.com/idea/download) installed along
with the [Gradle](https://www.jetbrains.com/help/idea/gradle.html) Plugin for Jetbrains installed
and configured, if not, do that first! If already done, proceed ahead.

#### Step - 2 : Fork and Clone

- Use your GitLab account to [fork](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/forks/new) this project
  - Don't know how forking works? Refer to [this guide](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html#doc-nav).
  - Don't have GitLab account? [Create one](https://gitlab.com/users/sign_up)! It is free and it is awesome!
- Visit your forked project (usually URL is `https://gitlab.com/<your user name>/editor-extensions/gitlab-jetbrains-plugin`).
- Set up pull mirroring to keep your fork up to date.
  - [How do I mirror repositories?](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pulling-from-a-remote-repository)
  - Use `https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin.git` as the **Git repository URL**.
- Go to your project overview and copy the SSH or HTTPS URL to clone the project into your system.
  - Don't know how to clone a project? Refer to [this guide](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html#clone-your-project).
- [Open](https://www.jetbrains.com/help/idea/import-project-or-module-wizard.html#open-project) your cloned project into Intellij IDEA.

#### Step - 3 : Run Plugin

Goto Intellij IDEA > Gradle > Run Configuration > Run Plugin

#### Step - 3 : Run Tests

Goto Intellij IDEA > Gradle > Run Configuration > Run Tests

#### Step - 4 : Add documentation

If you added or changed a feature, add the documentation to the README.

To add documentation that includes a new image:

1. Add images into the `docs/assets` folder, and commit the changes.
1. Edit the README file, and insert full permalinks to your new images.
   The permalinks contain the commit SHA from your first commit, and are
   in the form of:

   ```plaintext
   https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/raw/<COMMIT_SHA>/docs/assets/imagename.png
   ```

1. Commit your text changes.

### Adding new icons

If you're adding new icons to the plugin, you will need to put svg file named
exactly with the name of new icon in `src/main/resources/icons`.

### Opening Merge Requests

Steps to opening a merge request to contribute code to the GitLab Plugin for Jetbrains is similar to any other open source project.
You develop in a separate branch of your own fork and the merge request should have a related issue open in the project.
Any Merge Request you wish to open in order to contribute to the GitLab Plugin for Jetbrains, be sure you have followed through the steps from [Configuring Development Environment](#configuring-development-environment).
