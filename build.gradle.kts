import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.changelog.Changelog

// Read properties from gradle.properties to be used in here
fun properties(key: String) = project.findProperty(key).toString()

plugins {
  val kotlinVersion = "1.8.21"

  id("java")
  id("org.jetbrains.kotlin.jvm") version kotlinVersion
  id("org.jetbrains.kotlin.plugin.serialization") version kotlinVersion
  id("org.jetbrains.intellij") version "1.15.0"
  id("org.jetbrains.changelog") version "2.1.2"
}

group = "com.gitlab.plugin"
version = properties("plugin.version")

repositories {
  mavenCentral()
}

dependencies {
  val ktorVersion = properties("ktor.version")

  implementation("io.ktor:ktor-client-okhttp:$ktorVersion")
  implementation("io.ktor:ktor-client-auth:$ktorVersion")
  implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
  implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
  implementation("io.ktor:ktor-client-cio:$ktorVersion")
  implementation("io.ktor:ktor-client-logging:$ktorVersion")
  implementation("ch.qos.logback:logback-classic:1.4.8")
  implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.1")
  testImplementation("org.junit.jupiter:junit-jupiter-params:5.9.0")
  testImplementation("org.junit.jupiter:junit-jupiter:5.8.2")
  testImplementation("org.mockito:mockito-core:5.4.+")
  testImplementation("org.mockito.kotlin:mockito-kotlin:5.0.+")
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
  version.set(properties("platform.version"))
  type.set(properties("platform.type"))

  plugins.set(listOf("Git4Idea"))
}

// Configure Gradle Changelog Plugin - read more: https://github.com/JetBrains/gradle-changelog-plugin
changelog {
  version.set(properties("plugin.version"))
  groups.set(listOf("Added", "Changed", "Removed", "Fixed"))

  repositoryUrl.set(System.getenv("CI_PROJECT_URL") ?: "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin")
}

tasks {
  // Set the JVM compatibility versions
  withType<JavaCompile> {
    sourceCompatibility = properties("platform.java.version")
    targetCompatibility = properties("platform.java.version")
  }
  withType<KotlinCompile> {
    kotlinOptions {
      jvmTarget = properties("platform.java.version")
    }
  }

  wrapper {
    gradleVersion = properties("gradle.version")
  }

  patchPluginXml {
    sinceBuild.set(properties("plugin.minBuild"))
    untilBuild.set(properties("plugin.maxBuild"))

    changeNotes.set(provider {
      changelog.renderItem(
        changelog
          .getLatest()
          .withHeader(false)
          .withEmptySections(false),
        Changelog.OutputType.HTML
      )
    })
  }

  signPlugin {
    certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
    privateKey.set(System.getenv("PRIVATE_KEY"))
    password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
  }

  publishPlugin {
    token.set(System.getenv("PUBLISH_TOKEN"))
  }

  test {
    useJUnitPlatform()
  }
}
