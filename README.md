# [GitLab Plugin for JetBrains IDEs](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin) ([Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta))

This is the GitLab plugin for JetBrains IDEs (IntelliJ, PyCharm, GoLand, Webstorm, Rubymine, etc).

All feedback should be directed to [this issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/38).

## Minimum Supported Version

The GitLab plugin for JetBrains IDEs supports Code Suggestions for both [GitLab SaaS](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-gitlab-saas) and [GitLab self-managed](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-self-managed-gitlab).

This extension requires the **2023.2.X** release of JetBrains IDEs. These IDEs are currently in Early Access Preview and can be managed with the [JetBrains Toolbox App](https://www.jetbrains.com/toolbox-app/).

## Setup

This extension requires you to create a GitLab personal access token, and assign it to the extension:

1. [Install the extension]() from the JetBrains Plugin Marketplace (`Settings` -> `Plugins`) and enable it.
1. Create a [GitLab Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the `api` and `read_user` scopes:
1. Copy the token. _For security reasons, this value is never displayed again, so you must copy this value now._
1. Open Jetbrains IDE, then navigate to `Settings` -> `Tools` -> `GitLab`
   1. Provide the URL of your GitLab instance in the `GitLab URL` field. For GitLab SaaS, use `https://gitlab.com`.
   1. Paste the recently created token in to the `Access Token` field. The token is not displayed, nor is it accessible to others.

## Features

### Code Suggestions (Beta)

Write code more efficiently by using generative AI to suggest code while you’re developing. To learn more about this feature, see the
[Code Suggestions documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-in-vs-code)

This feature is in
[Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta)
Code Suggestions is a generative artificial intelligence (AI) model. GitLab currently leverages [Google Cloud’s Vertex AI Codey API models](https://cloud.google.com/vertex-ai/docs/generative-ai/code/code-models-overview)

No new additional data is collected to enable this feature. Private non-public GitLab customer data is not used as training data.
Learn more about [Google Vertex AI Codey APIs Data Governance](https://cloud.google.com/vertex-ai/docs/generative-ai/data-governance)

Beta users should read about the [known limitations](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#known-limitations)

#### Supported Languages

Languages supported by GitLab Duo Code Suggestions can be found in the [documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#supported-languages).

#### Usage

- `Tab` accepts suggestion
- `Escape` dismisses suggestion

#### Status Bar

A status icon is displayed in the status bar. It provides the following:

1. When token is not entered, disabled status icon will be shown.
1. When token is entered, enabled status icon will be shown.
1. When code suggestions tries to fetch suggestions, loading status icon will be shown.
1. When there is any exception in background, error status icon will be shown.

Report issues in the
[feedback issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/38).

---

## Contributing

This extension is open source and [hosted on GitLab](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin). Contributions are more than welcome and subject to the terms set forth in [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/CONTRIBUTING.md) -- feel free to fork and add new features or submit bug reports. See [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/CONTRIBUTING.md) for more information.
